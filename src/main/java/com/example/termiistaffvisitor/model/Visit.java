package com.example.termiistaffvisitor.model;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

@Data
@Entity
public class Visit{

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long visit_id;

    @ManyToOne
    private Staff staff;

    @ManyToOne
    private Visitor visitor;

//    private LocalDateTime date_of_visit = LocalDateTime.now();

    private String date_of_visit;

    private String reason_for_visit;
}
